# Regrowing Trees in Sons of the Forest

This simple script was written by me and ChatGPT. I am not a python programmer by trade so it could probably be much better. 

This will attempt to create a backup of the world each time you run it, but, please back it up manually if you're unsure. I am not responsible for any corruption to your save if you choose to run this script, it's at your own risk.

If you want this to work with multiplayer, the host will have to run the script. 

In order to use this, you need to have the game closed or be in the menu. You cannot run it as the game is running, since saving and leaving will overwrite the file with the current state in the game.

This script may break with updates in the future, or become obsolete if the feature is added in game.

## Features

- Regrow stumps only
- Regrow everything
- Unique backups

## Usage/Examples

You can create a simple .BAT script on windows to run the python code. Here's an example from my machine. You will have to obviously replace the path with your own paths.

```
python.exe RegrowTreeStumps.py ^
-p %LocalAppData%Low\Endnight\SonsOfTheForest\Saves\76561198211845748\Multiplayer\0738072606\WorldObjectLocatorManagerSaveData.json -stumps
```

You have a few options for command line arguments:

- `-p` is ***required***, and the following string needs to be the path to the save file. The file it needs to point to is **`WorldObjectLocatorManagerSaveData.json`**
- `-all` will regenerate all vegetation, trees and bushes. Maybe more as well. Anything this save file tracks will get wiped.
- `-stumps` will regenerate stumps and they regrow to trees. Fully cut down trees and bushes will not regenerate.
- `-gone` will regenerate everything that was originally gone in the save file. For instance, trees that were chopped down and stumps removed, as well as brush that was cleared. It will leave stumps alone.
- `-half_chopped` this option resets values that are serialized as 2. Which I don't really understand. When I tested, I noticed that hitting a tree and not cutting it down would serialize a value of "2". But honestly even with the value of 2, reloading into the map the trees are back to normal. Also, tree-chopping progress was not serialized. So this might not work.
- `-unique_backup` will tell the script to add a timestamp to your backup. This can quickly increase the save folder size, if you run the script often. It's off by default, resulting in just 1 backup that is overwritten.
## Authors

- [@david-arppe](https://www.gitlab.com/david-arppe)


