'''
This Python code is meant to modify a save file for the game "Sons of the Forest". It 
replaces serialized data for trees and shrubs. Trees that are chopped down (but with a 
stump remaining) can be regrown with this script.

Note that before running this script, it is highly recommended to backup your files as it
can potentially modify the save file in an irreversible way. This code should make a 
backup but just better be safe than sorry
'''

import json
import sys
import os
from enum import IntFlag, Enum, auto
from datetime import datetime

class VegetationState(IntFlag):
    NONE = 0
    GONE_ENTIRELY = 1
    HALF_CHOPPED = 2
    STUMPS = 4

def modify_file(filepath, objects_to_restore_mask, unique_backup):
    with open(filepath, 'r') as file:
        input_json = file.read()

    backup_filepath = f"{os.path.splitext(filepath)[0]}_backup.json"
    if unique_backup:
        backup_filepath = f"{os.path.splitext(filepath)[0]}_backup_{datetime.now().strftime('%H-%M-%S')}.json"
    
    with open(backup_filepath, 'w') as backup_file:
        backup_file.write(input_json)

    obj = json.loads(input_json)

    data = obj["Data"]
    world_object_locator_manager_prop = data["WorldObjectLocatorManager"]

    world_object_locator_manager_json = world_object_locator_manager_prop.replace('\\"', '"')
    world_object_locator_manager = json.loads(world_object_locator_manager_json)

    serialized_states = world_object_locator_manager["SerializedStates"]

    for i in range(len(serialized_states)-1, -1, -1):
        kvp = serialized_states[i]
        object_value = 1 << (kvp["Value"] - 1)

        if (object_value & objects_to_restore_mask) != 0:
            serialized_states.pop(i)

    world_object_locator_manager["SerializedStates"] = serialized_states

    world_object_locator_manager_json = json.dumps(world_object_locator_manager, separators=(',', ':'))
    data["WorldObjectLocatorManager"] = world_object_locator_manager_json

    output_json = json.dumps(obj, separators=(',', ':'))
    with open(filepath, 'w') as file:
        file.write(output_json)

if __name__ == "__main__":
    filepath = None
    vegetation_state = VegetationState.NONE
    unique_backup = False

    for i in range(len(sys.argv)):
        arg = sys.argv[i]
        if arg == "-p":
            if i < len(sys.argv) - 1 and os.path.isfile(sys.argv[i + 1]):
                filepath = sys.argv[i + 1]
                i += 1 # skip the next argument
            else:
                print("Invalid filepath argument.")
                sys.exit()
        elif arg == "-all":
            vegetation_state |= VegetationState.STUMPS | VegetationState.HALF_CHOPPED | VegetationState.GONE_ENTIRELY
        elif arg == "-stumps":
            vegetation_state |= VegetationState.STUMPS
        elif arg == "-half_chopped":
            vegetation_state |= VegetationState.HALF_CHOPPED
        elif arg == "-gone":
            vegetation_state |= VegetationState.GONE_ENTIRELY
        elif arg == "-unique_backup":
            unique_backup = True

    if filepath is None:
        print("Missing filepath argument.")
        sys.exit()

    modify_file(filepath, vegetation_state, unique_backup)